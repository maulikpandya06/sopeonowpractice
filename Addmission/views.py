from django.shortcuts import render, HttpResponse

# Create your views here.

def home(request):
      # return HttpResponse("Hello from views")
      context = {"title":"HomePage"}
      return render(request, 'Addmission/reports/home.html', context)

def contact(request):
      # return HttpResponse("Hello from views")
      context = {"title":"ContactPage"}
      return render(request, 'Addmission/reports/contact.html', context)

def about(request):
      context = {"title":"AboutPage"}
      return render(request, 'Addmission/reports/about.html', context)
